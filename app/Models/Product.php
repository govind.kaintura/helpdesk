<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'desc',
        'image',
        'type'
    ];

    protected $appends = [
        'imageUrl'
    ];

    /**
     * Accessors.
     *
     */
    public function getImageUrlAttribute()
    {
        return $this->attributes['imageUrl'] = $this->image ?  config('app.url') . $this->image : NULL;
    }
}
