/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);

import CKEditor from '@ckeditor/ckeditor5-vue';

Vue.use( CKEditor );

import moment from 'moment';
import { Form, HasError, AlertError } from 'vform';

import swal from 'sweetalert2';
window.swal = swal;

const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

window.toast = toast;

window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

import VueProgressBar from 'vue-progressbar';
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '2px'
});

import VueRouter from 'vue-router';
Vue.use(VueRouter);

let routes = [
    { path: '/dashboard', name: 'dashboard', component: require('./components/Dashboard.vue').default },
    { path: '/developer', component: require('./components/Developer.vue').default },
    { path: '/roles', component: require('./components/Roles.vue').default },
    { path: '/users', component: require('./components/Users.vue').default },
    { path: '/profile', component: require('./components/Profile.vue').default },
    { path: '/categories', component: require('./components/Category.vue').default },
    { path: '/products', component: require('./components/Product/Index.vue').default },
    { path: '/products/create', name: 'product-create', component: require('./components/product/Create.vue').default, props: false },
    { path: '/products/edit/:id', name: 'product-edit', component: require('./components/product/Edit.vue').default, props: true },
    { path: '/tickets', component: require('./components/Ticket.vue').default }
];


const router = new VueRouter({
    mode: 'history',
    base: window.location.dashboard,
    routes
});

Vue.filter('upText', function(text) {
    if (text) {
        return text.toUpperCase();
    }else{
        return 'error'
    }
});
Vue.filter('titleText', function(text) {
    if (text) {
        return text.replace(/(?:^|\s|-)\S/g, x => x.toUpperCase());
    }else{
        return 'error'
    }
});
Vue.filter('customDate', function(date) {
    return moment(date).format('MMMM Do YYYY');
});

window.Fire = new Vue();
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


Vue.component(
    'not-found',
    require('./components/NotFound.vue').default
);

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
