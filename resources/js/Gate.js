export default class Gate {

    constructor(user) {
        this.user = user;
    }

    isAdmin() {
        return this.user.roles[0].name === 'admin';
    }

    isAgent() {
        return this.user.roles[0].name === 'agent';
    }

    isClient() {
        return this.user.roles[0].name === 'client';
    }

    isAdminOrAuthor() {
        if (this.user.roles[0].name === 'admin' || this.user.roles[0].name === 'agent') {
            return true;
        }
    }
}
